import {Injectable} from 'angular2/core';

@Injectable()
export class Menu {
  public links:Array<Object> = [
    {label: 'Home', state: 'Home'},
    {label: 'Employees', state: 'Employees'},
    {label: 'Projects', state: 'Projects'}
  ];
}
