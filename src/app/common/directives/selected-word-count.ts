import {Directive, ElementRef, Input} from 'angular2/core';

@Directive({
  selector: '[selectedWordCount]',
  host: {
    '(mouseup)': 'onMouseUp()'
  }
})
export class SelectedWordCountDirective {
  constructor(private _el:ElementRef) {
  }

  onMouseUp() {
    let selectedText = this._getSelectionText();

    if (selectedText !== '') {
      let nodeText = this._el.nativeElement.textContent;
      let nodeTextArr = nodeText.split(' ');
      let count = 0;

      for (let i = 0, len = nodeTextArr.length; i < len; i++) {
        if (nodeTextArr[i] === selectedText) {
          count++;
        }
      }

      if (count > 0) {
        alert('Word "' + selectedText + '" appears ' + count + ' time' + (count > 1 ? 's' : '') + ' inside this paragraph!');
      }
    }
  }

  private _getSelectionText() {
    var text = '';
    if (window.getSelection) {
      text = window.getSelection().toString();
    }
    return text;
  }
}
