import {Pipe, PipeTransform} from 'angular2/core';

@Pipe({
  name: 'age'
})

export class AgePipe implements PipeTransform {
  transform(date: any) {
    if (date === null) {
      return date;
    }

    if (typeof date === 'string') {
      date = new Date(date);
    }

    let today:any = new Date();
    let age = today.getFullYear() - date.getFullYear();
    let m = today.getMonth() - date.getMonth();

    if (m < 0 || (m === 0 && today.getDate() < date.getDate())) {
      age--;
    }

    return age;
  }
}
