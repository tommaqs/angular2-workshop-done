import {Pipe, PipeTransform} from 'angular2/core';

var orderBy:Function = require('lodash/orderBy');

@Pipe({
  name: 'orderBy',
  pure: false
})

export class OrderByPipe implements PipeTransform {
  transform(input: Array<Object>, args:Array<string>):Array<Object> {
    let predicate = args[0];
    let order = args[1];

    if (order !== 'desc') {
      order = 'asc';
    }

    return orderBy(input, [predicate], [order]);
  }
}
