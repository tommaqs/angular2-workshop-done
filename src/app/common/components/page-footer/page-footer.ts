import {Component, View} from 'angular2/core';

@Component({
  selector: 'footer'
})
@View({
  styles: [`
    .footer {
      margin-top: 100px;
      bottom: 0px;
      width: 100%;
      height: 60px;
      line-height: 60px;
      background-color: #f5f5f5;
    }
  `],
  template: require('./page-footer.html')
})

export class PageFooter {}
