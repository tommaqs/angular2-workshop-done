import {Component, Input} from 'angular2/core';

@Component({
  selector: 'sort-indicator',
  template: require('./sort-indicator.html')
})

export class SortIndicator {
  @Input() predicateName;
  @Input() currentPredicate;
  @Input() order;
}
