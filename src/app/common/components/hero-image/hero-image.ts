import {Component, Input, View, OnInit} from 'angular2/core';

@Component({
  selector: 'hero-image'
})
@View({
  styles: [require('./hero-image.css')],
  template: require('./hero-image.html')
})

export class HeroImage implements OnInit {
  private maxTime:number = 3000;
  private interval;

  public heroText:string = '';
  public isHeroTyping:boolean = true;

  @Input() height;
  @Input() src;
  @Input() text;

  ngOnInit() {
    let textLength:number = this.text.length || 1;
    let intervalTime:number = this.maxTime / textLength;
    let iterator:number = 0;

    this.isHeroTyping = true;

    this.interval = setInterval(() => {
      this.heroText += this.text.substr(iterator, 1);
      iterator++;

      if (this.heroText.length >= this.text.length) {
        this.isHeroTyping = false;
        clearInterval(this.interval);
      }
    }, intervalTime);
  }
}
