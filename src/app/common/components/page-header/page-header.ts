import {Component, View} from 'angular2/core';
import {ROUTER_DIRECTIVES} from 'angular2/router';
import {Collapse} from 'ng2-bootstrap';

import {Menu} from '../../services/menu';
import {RouterActive} from '../../directives/router-active';

@Component({
  selector: 'page-header',
  providers: [
    Menu
  ]
})
@View({
  directives: [ ...ROUTER_DIRECTIVES, RouterActive, Collapse],
  template: require('./page-header.html')
})
export class PageHeader {
  public links:Array<Object> = [];
  public isCollapsed:boolean = false;

  constructor(public menu: Menu) {
    this.links = menu.links;
  }

  toggleCollapse() {
    this.isCollapsed = !this.isCollapsed;
  }
}
