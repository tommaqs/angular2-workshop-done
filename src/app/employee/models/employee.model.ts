export class Employee {
  id: number;
  firstName: String;
  lastName: String;
  dateOfBirth: any;
  title: Object;
  projects: Array<any>;

  constructor(id: number,
              firstName: string,
              lastName: string,
              dateOfBirth: any,
              projects: Array<any>,
              title: Object = {shortcut: 'n/a'}) {
    this.id = id;
    this.firstName = firstName;
    this.lastName = lastName;
    this.dateOfBirth = dateOfBirth ? new Date(dateOfBirth) : null;
    this.title = title;
    this.projects = projects;
  }
}
