export class Title {
  id: Number;
  name: String;
  shortcut: String;
  employees: Array<any>;

  constructor(id: Number,
              name: String,
              shortcut: String,
              employees: Array<any>) {
    this.id = id;
    this.name = name;
    this.shortcut = shortcut;
    this.employees = employees;
  }
}
