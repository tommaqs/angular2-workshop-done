import {Injectable} from 'angular2/core';
import {Http, Response, Headers, RequestOptions} from 'angular2/http';
import {Observable} from 'rxjs/Observable';

import {Employee} from '../models/employee.model';

@Injectable()
export class EmployeeService {
  constructor(private http:Http) {
  }

  createEmployee(employee) {
    let body = JSON.stringify(employee);
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });

    return this.http.post('http://localhost:3030/api/employees/', body, options)
      .map(res => res.json())
      .catch(this.handleError);
  }

  getEmployee(id) {
    return this.http.get('http://localhost:3030/api/employees/' + id + '?filter[include]=title')
      .map(res => res.json())
      .map((employee) => {
        return new Employee(
          employee.id, employee.firstName, employee.lastName,
          employee.dateOfBirth, employee.projects, employee.title
        );
      })
      .catch(this.handleError);
  }

  getEmployeeProjects(id) {
    return this.http.get('http://localhost:3030/api/employees/' + id + '/projects')
      .map(res => res.json())
      .catch(this.handleError);
  }

  getEmployees() {
    return this.http.get('http://localhost:3030/api/employees?filter[include]=title')
      .map(res => res.json())
      .map((employees:Array<any>) => {
        let result:Array<Employee> = [];
        if (employees) {
          employees.forEach((employee) => {
            result.push(
              new Employee(
                employee.id, employee.firstName, employee.lastName,
                employee.dateOfBirth, employee.projects, employee.title
              )
            );
          });
        }
        return result;
      })
      .catch(this.handleError);
  }

  removeEmployee(id) {
    return this.http.delete('http://localhost:3030/api/employees/' + id)
      .map(res => res.json())
      .catch(this.handleError);
  }

  private handleError(error:Response) {
    console.error(error);
    return Observable.throw(error.json().error || 'Server error');
  }
}
