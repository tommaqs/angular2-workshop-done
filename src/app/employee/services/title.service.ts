import {Injectable} from 'angular2/core';
import {Http, Response} from 'angular2/http';
import {Observable} from 'rxjs/Observable';

import {Title} from '../models/title.model';

@Injectable()
export class TitleService {
  constructor(private http:Http) {
  }

  getTitles() {
    return this.http.get('http://localhost:3030/api/titles?filter[include]=employees')
      .map(res => res.json())
      .map((titles:Array<any>) => {
        let result:Array<Title> = [];
        if (titles) {
          titles.forEach((title) => {
            result.push(
              new Title(
                title.id, title.name, title.shortcut, title.employees
              )
            );
          });
        }
        return result;
      })
      .catch(this.handleError);
  }

  private handleError(error:Response) {
    console.error(error);
    return Observable.throw(error.json().error || 'Server error');
  }
}
