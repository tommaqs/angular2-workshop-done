import {Component, Injector, OnInit} from 'angular2/core';
import {RouteParams} from 'angular2/router';
import {Employee} from '../models/employee.model';
import {EmployeeService} from '../services/employee.service';

@Component({
  selector: 'employee-details-view',
  providers: [EmployeeService],
  template: `
    <p>Projects:</p>
    <ul>
      <li *ngFor="#project of projects">{{project.name}}</li>
    </ul>
  `
})

export class EmployeeDetailsProjects implements OnInit {
  constructor(private _injector: Injector,
              private _employeeService:EmployeeService) {
  }

  public projects:Array<any>;

  ngOnInit() {
    let params = this._injector.parent.parent.get(RouteParams);
    let id = params.get('id');

    this._employeeService.getEmployeeProjects(id)
      .subscribe(
        projects => this.projects = projects);
  }
}
