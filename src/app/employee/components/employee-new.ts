import {Component, OnInit} from 'angular2/core';
import {NgForm} from 'angular2/common';

import {Employee} from '../models/employee.model';
import {Title} from '../models/title.model';
import {EmployeeService} from '../services/employee.service';
import {TitleService} from '../services/title.service';

@Component({
  selector: 'employee-new',
  providers: [EmployeeService, TitleService],
  template: require('./employee-new.html')
})

export class EmployeeNew implements OnInit {
  constructor(private _employeeService:EmployeeService,
              private _titleService:TitleService) {
  }

  // flow control
  public active:Boolean = true;
  public isSubmitting:Boolean = false;

  // models
  public employee:Object = {};
  public titles:Array<Title>;

  public onSubmit(employee) {
    this.isSubmitting = true;

    this._employeeService.createEmployee(employee)
      .subscribe(
      () => {
        this.employee = {};
        this.isSubmitting = false;

        // dirty hack for resetting the form
        this.active = false;
        setTimeout(()=> this.active=true, 0);
      });
  }

  ngOnInit () {
    this._titleService.getTitles()
      .subscribe(
        titles => this.titles = titles);
  }
}
