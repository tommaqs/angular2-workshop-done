import {Component, OnInit} from 'angular2/core';
import {RouteConfig, RouteParams, ROUTER_DIRECTIVES} from 'angular2/router';
import {Employee} from '../models/employee.model';
import {EmployeeService} from '../services/employee.service';

import {EmployeeDetailsView} from './employee-details-view';
import {EmployeeDetailsProjects} from './employee-details-projects';

@Component({
  selector: 'employee-details',
  directives: [ ...ROUTER_DIRECTIVES],
  providers: [EmployeeService],
  template: require('./employee-details.html')
})
@RouteConfig([
  { path: '/details', component: EmployeeDetailsView, name: 'EmployeeDetailsView', useAsDefault: true },
  { path: '/projects', component: EmployeeDetailsProjects, name: 'EmployeeDetailsProjects' }
])

export class EmployeeDetails implements OnInit {
  constructor(private _routeParams:RouteParams,
              private _employeeService:EmployeeService) {
  }

  public employee:Employee;

  ngOnInit() {
    let id = this._routeParams.get('id');

    this._employeeService.getEmployee(id)
      .subscribe(
        employee => this.employee = employee);
  }
}
