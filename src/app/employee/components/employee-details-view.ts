import {Component, Injector, OnInit} from 'angular2/core';
import {RouteParams} from 'angular2/router';
import {Employee} from '../models/employee.model';
import {EmployeeService} from '../services/employee.service';

@Component({
  selector: 'employee-details-view',
  providers: [EmployeeService],
  template: `
    <pre>{{employee|json}}</pre>
  `
})

export class EmployeeDetailsView implements OnInit {
  constructor(private _injector: Injector,
              private _employeeService:EmployeeService) {
  }

  public employee:Employee;

  ngOnInit() {
    let params = this._injector.parent.parent.get(RouteParams);
    let id = params.get('id');

    this._employeeService.getEmployee(id)
      .subscribe(
        employee => this.employee = employee);
  }
}
