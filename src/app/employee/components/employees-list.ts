import {Component, View, OnInit} from 'angular2/core';
import {ROUTER_DIRECTIVES} from 'angular2/router';
import {AgePipe} from '../../common/pipes/age.pipe';
import {OrderByPipe} from '../../common/pipes/orderBy.pipe';
import {SortIndicator} from '../../common/components/sort-indicator/sort-indicator';

import {EmployeeService} from '../services/employee.service';
import {Employee} from '../models/employee.model';

@Component({
  selector: 'employees-list',
  providers: [EmployeeService]
})
@View({
  directives: [...ROUTER_DIRECTIVES, SortIndicator],
  pipes: [AgePipe, OrderByPipe],
  template: require('./employees-list.html')
})

export class EmployeesList implements OnInit {
  constructor(private _employeeService:EmployeeService) {
  }

  public employees:Array<any> = [];
  public predicate:String = 'id';
  public order:String = 'asc';
  public errorMessage;

  public sort (predicate) {
    if (this.predicate === predicate) {
      this.order = this.order === 'asc' ? 'desc' : 'asc';
    } else {
      this.order = 'asc';
      this.predicate = predicate;
    }
  }

  public removeEmployee (employee) {
    let index = this.employees.indexOf(employee);

    if(window.confirm('Are you sure?')) {
      this._employeeService.removeEmployee(employee.id)
        .subscribe(() => {
          this.employees.splice(index,1);
        });
    }
  }

  ngOnInit() {
    this._employeeService.getEmployees()
      .subscribe(
        data => {
          this.employees = data;
          this.employees.forEach((employee) => {
            employee.age = new AgePipe().transform(employee.dateOfBirth);
          });
        },
        error => this.errorMessage = <any>error);
  }
}
