import {Component, View, OnInit} from 'angular2/core';
import {Alert} from 'ng2-bootstrap';
import {ProjectService} from '../services/project.service';

@Component({
  selector: 'projects-list',
  providers: [ProjectService]
})
@View({
  directives: [Alert],
  template: require('./projects-list.html')
})

export class ProjectsList implements OnInit {
  constructor(private _projectService:ProjectService) {
  }

  public alert:Object = {
    type: 'info',
    msg: 'There is no any project here. You should consider adding one.'
  };
  public projects:Array<Object> = [];
  public errorMessage;

  ngOnInit() {
    this._projectService.getProjects()
      .subscribe(
        data => this.projects = data,
        error => this.errorMessage = <any>error);
  }
}
