import {Injectable} from 'angular2/core';
import {Http, Response} from 'angular2/http'
import {Observable} from 'rxjs/Observable';

@Injectable()
export class ProjectService {
  constructor(private http:Http) {
  }

  getProjects() {
    return this.http.get('http://localhost:3030/api/projects')
      .map(res => res.json())
      .catch(this.handleError);
  }

  private handleError (error: Response) {
    console.error(error);
    return Observable.throw(error.json().error || 'Server error');
  }
}
