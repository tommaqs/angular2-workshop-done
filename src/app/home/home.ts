import {Component, OnInit} from 'angular2/core';

import {PageTitle} from './services/page-title';
import {HeroImage} from '../common/components/hero-image/hero-image';
import {SelectedWordCountDirective} from '../common/directives/selected-word-count';

@Component({
  selector: 'home',
  directives: [HeroImage, SelectedWordCountDirective],
  providers: [PageTitle],
  styles: [require('./home.css')],
  template: require('./home.html')
})
export class Home implements OnInit {
  constructor(private _pageTitleService:PageTitle) {
  }

  public title:String = this._pageTitleService.getTitle() + ' - Homepage';

  ngOnInit() {
    console.log('hello ng-Mates!');
  }
}
