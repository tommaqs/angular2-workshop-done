import {Component} from 'angular2/core';
import {RouteConfig, ROUTER_DIRECTIVES} from 'angular2/router';
import {FORM_PROVIDERS} from 'angular2/common';
import {Alert} from 'ng2-bootstrap/ng2-bootstrap';

import {Home} from './home/home';
import {PageHeader} from './common/components/page-header/page-header';
import {PageFooter} from './common/components/page-footer/page-footer';

import {EmployeesList} from './employee/components/employees-list';
import {EmployeeDetails} from './employee/components/employee-details';
import {EmployeeNew} from './employee/components/employee-new';
import {ProjectsList} from './project/components/projects-list';

/*
 * App Component
 * Top Level Component
 */
@Component({
  selector: 'app',
  providers: [ ...FORM_PROVIDERS ],
  directives: [ ...ROUTER_DIRECTIVES, PageHeader, PageFooter],
  pipes: [],
  styles: [`
    .splash {
      padding-top: 50px;
    }
  `],
  template: require('./app.html')
})
@RouteConfig([
  { path: '/', component: Home, name: 'Index' },
  { path: '/home', component: Home, name: 'Home' },
  { path: '/employees', component: EmployeesList, name: 'Employees' },
  { path: '/employee/:id/...', component: EmployeeDetails, name: 'EmployeeDetails' },
  { path: '/employee/new', component: EmployeeNew, name: 'EmployeeNew' },
  { path: '/projects', component: ProjectsList, name: 'Projects' },
  { path: '/**', redirectTo: ['Index'] }
])
export class App {}
